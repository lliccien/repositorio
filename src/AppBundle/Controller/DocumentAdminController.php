<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;



class DocumentAdminController extends CRUDController
{

    public function downloadDocumentAction()
    {

        $filename = $this->admin->getSubject()->getDocument();
        //ldd($document);

        $request = $this->get('request');
        //$path = $this->admin->getSubject()->getUploadRootDir();
        //$path = '/var/www/html/web/uploads/documents/';
        $path = $this->getRequest()->server->get('DOCUMENT_ROOT')."/web/uploads/documents/";
        $content = file_get_contents($path.$filename);

        $response = new Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

        $response->setContent($content);
        return $response;

    }

    public function sendEmailAction(Request $request = null)
    {


        $form = $this->createFormBuilder()
            ->add('client', 'entity', array(
                'class' => 'AppBundle\Entity\Client',
                'choice_label' => 'denomination',
                'label' => 'Cliente'))
            ->add('messageSubject', 'text', array('label' => 'Asunto', 'required' => true))
            ->add('messageBody', 'textarea', array('label' => 'Mensaje', 'required' => true))

            ->getForm();

        $rq = $this->getRequest();
        $form->handleRequest($rq);

        if ($form->isSubmitted() && $form->isValid()) {

            $mclient = $form["client"]->getData()->getEmail();
            $msubject = $form["messageSubject"]->getData();
            $mbody = $form["messageBody"]->getData();

            $message = \Swift_Message::newInstance()
                ->setSubject($msubject)
                ->setFrom($mclient)
                ->setTo('pisistrato@gmail.com')
                ->setBody(
                    $this->renderView(
                        ':Email:message_email.html.twig',
                        array('mbody' => $mbody)
                    )

                );
            $message->setContentType('text/html');
            $this->get('mailer')->send($message);

            // send flahMessages
            $this->addFlash('sonata_flash_success','flash_create_success','El correo se envio Satisfactoriamente');


            return new RedirectResponse($this->admin->generateUrl('sendEmail'));
        }


        return $this->render($this->admin->getTemplate('sendEmail'), array('form' => $form->createView(),'action' => 'sendEmail', 'action' => 'sendEmail'), $response = null);


    }


}
