<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Document;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Document controller.
 *
 * @Route("document")
 */
class DocumentController extends Controller
{
    /**
     * Lists all document entities.
     *
     * @Route("/", name="document_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $owner= $this->container->get('security.context')->getToken()->getUser()->getClients()->first();
        $type = 1;
        $em = $this->getDoctrine()->getManager();

        $documents = $em->getRepository('AppBundle:Document')->findAllByOwnerAndType($owner, $type);

        return $this->render('document/index.html.twig', array(
            'documents' => $documents,
        ));
    }

    /**
     * Lists all document resume entities.
     *
     * @Route("/resume", name="document_resume")
     * @Method("GET")
     */
    public function resumeAction()
    {
        $owner= $this->container->get('security.context')->getToken()->getUser()->getClients()->first();
        $type = 1;
        $em = $this->getDoctrine()->getManager();

        $documents = $em->getRepository('AppBundle:Document')->findAllByOwnerAndType($owner, $type);

        return $this->render('document/resume.html.twig', array(
            'documents' => $documents,
            'title' => 'Listado de Fichas',
        ));
    }

    /**
     * Lists all document consolidate entities.
     *
     * @Route("/consolidate", name="document_consolidate")
     * @Method("GET")
     */
    public function consolidateAction()
    {
        $owner= $this->container->get('security.context')->getToken()->getUser()->getClients()->first();
        $type = 2;
        $em = $this->getDoctrine()->getManager();

        $documents = $em->getRepository('AppBundle:Document')->findAllByOwnerAndType($owner, $type);

        return $this->render('document/index.html.twig', array(
            'documents' => $documents,
            'title' => 'Listado de Consolidados',
        ));
    }



    /**
     * Creates a new document entity.
     *
     * @Route("/new", name="document_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $document = new Document();
        $form = $this->createForm('AppBundle\Form\DocumentType', $document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //capture data
            $createdby = $this->container->get('security.context')->getToken()->getUser();
            $owner = $this->container->get('security.context')->getToken()->getUser()->getClients()->first();
            $type = 2; //value to document consolidate
            $emailfrom = $owner->getEmail();
            //ld($owner->getEmail());

          // La variable $file guardará el archivo subido
            /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $document->getDocument();

            // Generar un numbre único para el archivo antes de guardarlo
            $fname = str_replace(" ", "-", $document->getDenomination());

            $fileName = $fname.'-'.time().'.'.$file->guessExtension();

            // Mover el archivo al directorio donde se guardan los documentos
            $file->move("uploads/documents", $fileName);

            // Actualizar la propiedad curriculum para guardar el nombre de archivo PDF
            // en lugar de sus contenidos
            $document->setDocument($fileName);

            //set data
            $document->setOwner($owner);
            $document->setCreatedBy($createdby);
            $document->setTypeDocument($type);

            $denomination = $document->getDenomination();

            // send email notification
            $this->sendEmail($owner,$denomination,$emailfrom);

            //ldd($document->getDenomination());
            $em = $this->getDoctrine()->getManager();
            $em->persist($document);
            $em->flush($document);


            // send flahMessages

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'success',
                    'title' => 'Success!',
                    'message' => 'El documento se subio satisfatoriamente.'

                )
            );

            return $this->redirectToRoute('document_consolidate');
        }

        return $this->render('document/new.html.twig', array(
            'document' => $document,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a document entity.
     *
     * @Route("/{id}", name="document_show")
     * @Method("GET")
     */
    public function showAction(Document $document)
    {
        $deleteForm = $this->createDeleteForm($document);

        return $this->render('document/show.html.twig', array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing document entity.
     *
     * @Route("/{id}/edit", name="document_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Document $document)
    {
        $deleteForm = $this->createDeleteForm($document);
        $editForm = $this->createForm('AppBundle\Form\DocumentType', $document);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_edit', array('id' => $document->getId()));
        }

        return $this->render('document/edit.html.twig', array(
            'document' => $document,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a document entity.
     *
     * @Route("/{id}", name="document_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Document $document)
    {
        $form = $this->createDeleteForm($document);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($file = 'uploads/documents/'.$document->getdocument($document)) {

                unlink($file);
            }


            $em = $this->getDoctrine()->getManager();
            $em->remove($document);
            $em->flush($document);

            // send flahMessages

            $this->get('session')->getFlashBag()->add(
                'notice',
                array(
                    'alert' => 'danger',
                    'title' => 'Cuidado!',
                    'message' => 'El documento se borrado satisfatoriamente.'

                )
            );

        }

        return $this->redirectToRoute('document_consolidate');
    }

    /**
     * Creates a form to delete a document entity.
     *
     * @param Document $document The document entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Document $document)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('document_delete', array('id' => $document->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    public function sendEmail($owner, $denomination, $emailfrom)
    {


            $mclient = $owner;
            $msubject = 'El cliente '.$mclient.' subio al respositorio el documento '.$denomination;
            $mbody = 'El cliente queda a la espera de sus buenos oficios';

            $message = \Swift_Message::newInstance()
                ->setSubject($msubject)
                ->setFrom($emailfrom)
                ->setTo('requerimientos@druckerc.com.pe')
                ->setBody(
                    $this->renderView(
                        'Email/message_notication.html.twig',
                        array('mbody' => $mbody)
                    )

                );
            $message->setContentType('text/html');
            $this->get('mailer')->send($message);

    }

    /**
     * Finds and download a document entity.
     *
     * @Route("/download/{filename}", name="document_download")
     * @Method("GET")
     */
    public function downloadAction($filename)
    {
        $request = $this->get('request');
        $path = $this->getRequest()->server->get('DOCUMENT_ROOT')."/web/uploads/documents/";
            //$this->get('kernel')->getRootDir(). "../web/uploads/documents";
        $content = file_get_contents($path.$filename);

        $response = new Response();

        //set headers
        $response->headers->set('Content-Type', 'mime/type');
        $response->headers->set('Content-Disposition', 'attachment;filename="'.$filename);

        $response->setContent($content);
        return $response;
    }


}
