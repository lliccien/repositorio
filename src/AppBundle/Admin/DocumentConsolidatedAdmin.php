<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;



class DocumentConsolidatedAdmin extends AbstractAdmin
{

protected $baseRouteName = 'document_consolidate';
protected $baseRoutePattern = 'document-consolidate';
 

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('downloadDocument');
    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

            ->add('owner', null, array('label' => 'Cliente'))
            ->add('denomination', null, array('label' => 'Denominación'))
            ->add('typeDocument', null, array('label' => 'Tipo de Documento'))
            ->add('createdAt', null, array('label' => 'Fecha de creado'))
            ->add('uploadedAt', null, array('label' => 'Fecha de subido'))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('owner', 'entity', array('label' => 'Cliente'))
            ->add('denomination', null, array('label' => 'Denominación'))
            ->add('document', null, array('label' => ' Documento'))
            ->add('typeDocument', null, array('label' => 'Tipo de Documento', 'template' => ':Admin:field_typeDocument.html.twig'))
            ->add('Descargar', 'string', array('template' =>':Admin:field_download.html.twig'))
            ->add('createdAt', null, array('label' => 'Fecha de creado'))
            ->add('uploadedAt', null, array('label' => 'Fecha de subido'))
            ->add('_action', null, array(
                'actions' => array(
                    //'show' => array(),
                    //'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            //->add('id')
            ->add('owner', 'entity', array(
                'class' => 'AppBundle\Entity\Client',
                'choice_label' => 'denomination',
                'label' => 'Cliente'))
            ->add('denomination', null, array('label' => 'Denominación'))
            ->add('file', 'file', array('label' => 'Documento'))
            ->add('typeDocument', 'choice', array('choices' => array('1' => 'Ficha',
                                                                    '2' => 'Consolidado'),
                                            'expanded' => true,
                                            'multiple' => false,
                                            'data'     => '2' ))
            //->add('createdAt')
            ->add('uploadedAt', null, array('label' => 'Fecha de subido'))
        ;
    }

    public function prePersist($document) {
        $this->saveFile($document);
        // trae el usuario actual logueado y lo setea en creadtedby
        $document->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
    }

    public function preUpdate($document) {
        $this->saveFile($document);
        $document->setCreatedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
        //ldd($this->getForm()->getData()->getDocument());
    }

    public function saveFile($document) {

        $document->upload();
    }

    function postRemove($document){
        $file =  $document->getAbsolutePath();
        @unlink($file);
    }


    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('owner', null, array('label' => 'Cliente'))
            ->add('denomination', null, array('label' => 'Denominación'))
            ->add('document', null, array('Label' => ' Documento'))
            ->add('createdBy', null, array('label' => 'Creado por'))
            ->add('createdAt', null, array('label' => 'Fecha de creado'))
            ->add('uploadedAt', null, array('label' => 'Fecha de subido'))
        ;
    }


    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $rootAlias = $query->getRootAliases()[0];
        $query
            ->andWhere(
                $query->expr()->eq($rootAlias.'.typeDocument', ':type')
            )            
            ;
        
        $query->setParameter('type', '2');
        return $query;
    }


   /**
     * Default Datagrid values
     *
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,            // display the first page (default = 1)
        '_sort_order' => 'DESC', // reverse order (default = 'ASC')
        '_sort_by' => 'uploadedAt'  // name of the ordered field
                                 // (default = the model's id field, if any)

        // the '_sort_by' key can be of the form 'mySubModel.mySubSubModel.myField'.
    );


}
